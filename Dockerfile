FROM node:14 as ui-modules

#ARG NPM_REGISTRY=https://artifactory.iponweb.net/artifactory/api/npm/uworkflow-npm-virtual
#ARG NPM_ALWAYS_AUTH=true
#ARG NPM_EMAIL=uworkflow-build-bot@iponweb.net
#ARG NPM_AUTH_KEY

#RUN test -n "$NPM_REGISTRY"
#RUN test -n "$NPM_ALWAYS_AUTH"
#RUN test -n "$NPM_EMAIL"
#RUN test -n "$NPM_AUTH_KEY"
# Add jenkins user which will do all tests.
# We have to do it because chrome can't be run under the root.
#RUN apt-get update && apt-get install -y sudo \
#    && adduser --disabled-password --gecos '' jenkins \
#    && adduser jenkins sudo \
#    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

#USER jenkins

# Configure npm client to be able to run it under non root user
#RUN mkdir -p /home/jenkins/.npm-global
#ENV NPM_CONFIG_PREFIX="/home/jenkins/.npm-global"
#ENV PATH=/home/jenkins/.npm-global/bin:$PATH
#ENV NODE_PATH='/home/jenkins/.npm-global'

#RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - \
#    && sudo echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google.list \
#    && sudo apt-get update \
#    && sudo apt-get install -y google-chrome-stable xvfb

# Configure npm client, update & install grunt-cli
RUN npm install -g npm grunt-cli

#COPY --chown=jenkins:jenkins package.json /opt/uworkflow/ui-modules/
#COPY --chown=jenkins:jenkins package-lock.json /opt/uworkflow/ui-modules/
#COPY --chown=jenkins:jenkins postinstall.sh /opt/uworkflow/ui-modules/
#
#RUN cd /opt/uworkflow/ui-modules \
#    && SKIP_POSTINSTALL="true" npm ci

#COPY --chown=jenkins:jenkins . /opt/uworkflow/ui-modules

RUN cd /opt/uworkflow/ui-modules \
    && grunt build

